package vn.vimaru.fit.ttm.introspring.introspring.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Fibonacci {

  @Autowired
  private FibonacciService fbService;

  @RequestMapping("/fibonacci/{n}")
  public long fibonacci(@PathVariable long n) {
    return fbService.fibonacci(n); 
  }
}