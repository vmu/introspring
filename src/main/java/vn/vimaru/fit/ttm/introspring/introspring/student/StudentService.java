package vn.vimaru.fit.ttm.introspring.introspring.student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

  @Autowired
  StudentRepository stRepository;

  // Lấy thông tin học sinh có msv tương ứng
  public Optional<Student> getStudent(String msv) {
    return stRepository.findById(msv);
  }

  // Cập nhật thông tin học sinh
  public void updateStudent(String msv, Student st) {
    st.setMsv(msv);
    stRepository.save(st);
  }

  // Tạo thêm một học sinh
  public void createStudent(String msv, Student st) {
    st.setMsv(msv);
    stRepository.save(st);
  }

  // Lấy danh sách toàn bộ các học sinh
  public List<Student> getAllStudents() {
    List<Student> students = new ArrayList<Student>();
    stRepository.findAll().forEach(students::add);
    return students;
  }

  // Tìm kiếm học sinh theo tên
  public List<Student> findStudentByName(String name) {
    List<Student> students = new ArrayList<>();
    stRepository.findByName(name).forEach(students::add);
    return students;
  }

  // Tìm kiếm theo tên hoặc msv
  public List<Student> findStudentByNameOrMsv(String name_msv) {
    return stRepository.findByNameOrMsv(name_msv, name_msv);
  }
}